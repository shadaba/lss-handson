#Author: Shadab Alam, October 2024 for illustration of fisher forecast
import numpy as np
import matplotlib.pyplot as plt
#from scipy.integrate import quad
from scipy.constants import c  # speed of light in m/s
#import corner
from chainconsumer import Chain, ChainConfig, ChainConsumer, PlotConfig, Truth, make_sample
import pylab as pl
from scipy import integrate
from scipy.interpolate import interp1d
import hashlib


#The range of reshift considered
global interp
interp={'zmin':0.01,'zmax':5,'nz':1000}
# Cosmological parameters
H0_fid = 70  # km/s/Mpc
Omegam_fid = 0.3
w0_fid = -1.0
wa_fid = 0.0
Omegak_fid = 0.0

# Function to calculate E(z)
def E(z, H0, Omegam, w0, wa, Omegak):
    Omegade = 1 - Omegam - Omegak
    a=1/(1+z)
    wde=w0 + (1-a)*wa
    return H0*np.sqrt(Omegam * (1 + z)**3 + Omegak * (1 + z)**2 + 
                   Omegade * (1 + z)**(3 * (1 + wde)))


def comoving_distance(z, H0=79, Omegam=0.3, w0=-1, wa=0, Omegak=0, interp=None):
    c = 299792.458  # speed of light in km/s

    #def E(z):
    #    Omegade = 1 - Omegam - Omegak
    #    a=1/(1+z)
    #    wde=w0 + (1-a)*wa
    #    return np.sqrt(Omegam * (1 + z)**3 + Omegak * (1 + z)**2 + 
    #               Omegade * (1 + z)**(3 * (1 + wde)) )
    #def E(z):
    #    return np.sqrt(omega_m * (1 + z)**3 + omega_l * (1 + z)**(3 * (1 + w)))

    #def integrand(z):
    #    return c*1e-2 / E(z)

    def distance(zarr,H0,Omegam,w0,wa,Omegak):
        if(zarr.min()>0):
            xx=np.append(0,zarr)
        else:
            xx=zarr
        Ez=E(xx,H0,Omegam,w0,wa,Omegak)
        integrand= c/Ez
        res=integrate.cumulative_trapezoid(integrand,x=xx,initial=0)

        if(zarr.min()>0):
            res=res[1:]

        #pl.plot(zarr,res)
        #pl.show()
        return res

    if interp is None:
        # Calculate distance without interpolation
        integral, _ = integrate.quad(integrand, 0, z)
        d = c * integral / h
        return d, interp
        

    # Create a hashcode for the parameters
    params = f"{Omegam}_{H0}_{w0}_{wa}_{Omegak}"
    hashcode = hashlib.md5(params.encode()).hexdigest()
  
    #print(z,H0,Omegam,w0,wa,Omegak)
    #print('check :', distance(np.array([3]),H0,Omegam,w0,wa,Omegak))
    #print('check-5 :', distance(np.array([3]),H0-5,Omegam,w0,wa,Omegak))

    # Check if interp dictionary contains required parameters
    if all(key in interp for key in ['zmin', 'zmax', 'nz']):
        zmin, zmax, nz = interp['zmin'], interp['zmax'], interp['nz']
        
        if hashcode not in interp:
            # Create interpolation if it doesn't exist
            zbins = np.linspace(zmin, zmax, nz)
            distances = distance(zbins,H0,Omegam,w0,wa,Omegak)
            #distances = np.array([distance(zi, omega_m, omega_l, h, w)[0] for zi in zbins])
            interp_func = interp1d(zbins[1:], distances[1:], kind='linear')
            interp[hashcode] = {'zbins': zbins[:], 'distances': distances[:],'ifunc':interp_func}
            print('setup:' ,hashcode, params)

        # Interpolate the distance
        #print('interp: ',params, hashcode)
        #interp_func = interp1d(interp[hashcode]['zbins'][:], interp[hashcode]['distances'][:], kind='linear')
        #print(params,interp[hashcode]['zbins'][-10],interp[hashcode]['distances'][-10])
        #d = interp_func(z)  # Convert to float to ensure it's a scalar
        d= interp[hashcode]['ifunc'](z)
        return d, interp
    else:
        # If interp doesn't have required parameters, calculate without interpolation
        integral, _ = integrate.quad(integrand, 0, z)
        d = c * integral / h
        return d, interp

# Function to calculate comoving distance
#def comoving_distance(z, H0, Omegam, w0, wa, Omegak,interp=None):
#    integrand = lambda x: 1 / E(x, H0, Omegam, w0, wa, Omegak)
#    return (c / 1000) * quad(integrand, 0, z)[0] / H0  # in Mpc

# Function to calculate angular diameter distance
def angular_diameter_distance(z, H0, Omegam, w0, wa, Omegak):
    global interp
    dc, interp = comoving_distance(z, H0, Omegam, w0, wa, Omegak,interp=interp)
    if Omegak > 1e-7:
        return dc * np.sinh(np.sqrt(Omegak) * H0 * dc / c) / (np.sqrt(Omegak) * H0 * dc / c) / (1 + z)
    elif Omegak < -1e-7:
        return dc * np.sin(np.sqrt(-Omegak) * H0 * dc / c) / (np.sqrt(-Omegak) * H0 * dc / c) / (1 + z)
    else:
        return dc / (1 + z)

# Function to calculate luminosity distance
def luminosity_distance(z, H0, Omegam, w0, wa, Omegak):
    return (1 + z) * angular_diameter_distance(z, H0, Omegam, w0, wa, Omegak)

# Function to calculate Hubble distance
def hubble_distance(z, H0, Omegam, w0, wa, Omegak):
    return  H0 * E(z, H0, Omegam, w0, wa, Omegak)

# Fisher matrix calculation
def fisher_matrix(z_bins, rd_bbn, errors, fixed_params=None,interp={}):
    params = ['Omegam', 'H0', 'w0', 'wa', 'Omegak', 'rd']
    if fixed_params:
        params = [p for p in params if p not in fixed_params]
    n_params = len(params)
    n_bins = len(z_bins)
    F = np.zeros((n_params, n_params))

    def calc_derivatives(z, param, interp):
        h = 1e-3
        fiducial = [ H0_fid, Omegam_fid,  w0_fid, wa_fid, Omegak_fid, rd_bbn]
        plus = fiducial.copy()
        minus = fiducial.copy()
        idx = params.index(param)
        plus[idx] += h
        minus[idx] -= h
        #print(param,idx,h)

        #setup things for interpolation
        dc, interp = comoving_distance(z, *fiducial[:5],interp=interp)
        dc, interp = comoving_distance(z, *plus[:5],interp=interp)
        dc, interp = comoving_distance(z, *minus[:5],interp=interp)

        #print('DA plus')
        DA_plus = angular_diameter_distance(z, *plus[:5])
        #print('DA minus')
        DA_minus = angular_diameter_distance(z, *minus[:5])
        H_plus = hubble_distance(z, *plus[:5])
        H_minus = hubble_distance(z, *minus[:5])
        DL_plus = luminosity_distance(z, *plus[:5])
        DL_minus = luminosity_distance(z, *minus[:5])

        dDA_dp = (DA_plus - DA_minus) / (2.0 * h)
        dH_dp = (H_plus - H_minus) / (2.0 * h)
        dDL_dp = (DL_plus - DL_minus) / (2.0 * h)
        #pl.plot(z,DA_plus - DA_minus)
        #pl.show()

        return dDA_dp, dH_dp, dDL_dp, interp


    DA_fid = angular_diameter_distance(z_bins, H0_fid, Omegam_fid, w0_fid, wa_fid, Omegak_fid)
    H_fid = hubble_distance(z_bins, H0_fid, Omegam_fid, w0_fid, wa_fid, Omegak_fid)
    DL_fid = luminosity_distance(z_bins, H0_fid, Omegam_fid, w0_fid, wa_fid, Omegak_fid)

    for a, param_a in enumerate(params):
        dDA_a, dH_a, dDL_a, interp = calc_derivatives(z_bins, param_a,interp)
        for b, param_b in enumerate(params):
            dDA_b, dH_b, dDL_b ,interp = calc_derivatives(z_bins, param_b,interp)
            F[a,b]=0
            if errors.get('DA_rd') is not None:
                F[a, b] =F[a, b]+np.sum(  (dDA_a / DA_fid) * (dDA_b / DA_fid) / errors['DA_rd']**2 )
            if errors.get('rd_H') is not None:
                F[a, b] = F[a, b]+np.sum( (dH_a / H_fid) * (dH_b / H_fid) / errors['rd_H']**2)
            if errors.get('DL') is not None:
                F[a, b] = F[a, b]+np.sum((dDL_a / DL_fid) * (dDL_b / DL_fid) / errors['DL']**2)
    if(False):
        print(interp.keys())
        tkey0=None
        for tt,tkey in enumerate(interp.keys()):
            if(tkey not in ['zmin','zmax','nz']):
               if(tkey0 is None):
                   tkey0=tkey
                   d0=interp[tkey]['ifunc'](interp[tkey0]['zbins'])
               pl.plot(interp[tkey]['zbins'],interp[tkey]['distances']-interp[tkey0]['distances'],'--',label=tkey)
               d1=interp[tkey]['ifunc'](interp[tkey]['zbins'])

               pl.plot(interp[tkey]['zbins'],d1-d0,'-',label=tkey)
        pl.legend()
        pl.show()


    # Add BBN prior on rd if it's not fixed and the error is provided
    if 'rd' in params and errors.get('rd_BBN') is not None:
        rd_index = params.index('rd')
        F[rd_index, rd_index] += 1 / errors['rd_BBN']**2

    return F, interp


# Function to run forecast and generate triangle plot
def run_forecast_and_plot(z_bins, rd_bbn=147.5, default_error=0.01, custom_errors=None, fixed_params=None, label='',interp={}):
    errors = {
        'DA_rd': np.full(len(z_bins), default_error),
        'rd_H': np.full(len(z_bins), default_error),
        'DL': np.full(len(z_bins), default_error),
        'rd_BBN': default_error
    }
    if custom_errors:
        errors.update(custom_errors)

    F , interp= fisher_matrix(z_bins, rd_bbn, errors, fixed_params, interp)
    cov = np.linalg.inv(F)
    
    params = ['Omegam', 'H0', 'w0', 'wa', 'Omegak', 'rd']
    if fixed_params:
        params = [p for p in params if p not in fixed_params]
    fiducial = [Omegam_fid, H0_fid, w0_fid, wa_fid, Omegak_fid, rd_bbn]
    fiducial = [fiducial[params.index(p)] for p in params]

    return params, fiducial, cov, label , interp

# Function to create triangle plot
def create_triangle_plot(results):
    n_params = len(results[0][0])
    fig, axes = plt.subplots(n_params, n_params, figsize=(3*n_params, 3*n_params))
    
    for params, fiducial, cov, label in results:
        corner.corner(cov, bins=50, smooth=0.8, labels=params, 
                      truths=fiducial, truth_color='r', 
                      plot_datapoints=False, fill_contours=True, 
                      levels=(0.68, 0.95), color=plt.get_cmap('tab10')(results.index((params, fiducial, cov, label))), 
                      fig=fig, label=label)

    plt.legend(loc='upper right', bbox_to_anchor=(0.95, 0.95))
    plt.tight_layout()
    plt.show()


def DL_constraints(interp):
    results=[]
    zlims=[0.1,2.0]
    nz=[50,100,500,1000]
    DL_err=0.01
   
    for ii in range(len(nz)):
        z_bins=np.linspace(zlims[0],zlims[1],nz[ii])
        tres=run_forecast_and_plot(z_bins, custom_errors={'DL': np.zeros(z_bins.size)+DL_err, 'rd_BBN': None,'DA_rd':None,
            'rd_H':None},fixed_params=['Omegak'], label='DL (%d)'%nz[ii] ,interp=interp)
        results.append(tres[:-1])
        interp=tres[-1]

    c = ChainConsumer()
    for params, fiducial, cov, label in results:
        #print(res[2],res[2].dtype,res[0],res[3])
        chain1 = Chain.from_covariance(fiducial,cov ,columns=params,name=label, linestyle=":")
        c.add_chain(chain1)
    fig = c.plotter.plot()

    pl.savefig('fisher_DL.png')
    pl.show()
    #create_triangle_plot(results)

# Main function to run all cases
def run_all_cases(interp):
    z_bins = np.linspace(0.1, 3.0, 20)
    results = []

    ind_DL=z_bins<2.0
    ind_DA=(z_bins>0) *(z_bins<0.5)
   
    fact_DL=np.zeros(z_bins.size)+0.01;fact_DL[~ind_DL]=100
    fact_DA=np.zeros(z_bins.size)+0.01;fact_DA[~ind_DA]=100
    fact_rdH=np.zeros(z_bins.size)+0.02;fact_DA[~ind_DA]=100

    errors = {
        'DA_rd':  fact_DA,
        'rd_H': fact_rdH,
        'DL': fact_DL,
        'rd_BBN': None
    }

    # Case 1: DA/rd and rd/H only
    tres=run_forecast_and_plot(z_bins, custom_errors={'DL': None, 'rd_BBN': None,'DA_rd':errors['DA_rd'],
        'rd_H':errors['rd_H']}, 
                                         fixed_params=['Omegak'], label='DA/rd + rd/H',interp=interp)
    interp=tres[-1]
    results.append(tres[:-1])

    # Case 2: DL only
    tres=run_forecast_and_plot(z_bins, custom_errors={'DA_rd': None, 'rd_H': None, 'rd_BBN': None, 'DL':errors['DL']}, 
                                         fixed_params=['Omegak'], label='DL only',interp=interp)

    interp=tres[-1]
    results.append(tres[:-1])
    # Case 3: DA/rd, rd/H with BBN
    #results.append(run_forecast_and_plot(z_bins, custom_errors={'DL': None,'rd_BBN':errors['rd_BBN'],'DA_rd':errors['DA_rd'],
    #    'rd_H':errors['rd_H']},  fixed_params=['Omegak'], label='DA/rd + rd/H + BBN'))

    # Case 4: All observables
    tres=run_forecast_and_plot(z_bins,custom_errors=errors, fixed_params=['Omegak'], label='All observables',interp=interp)
    interp=tres[-1]
    results.append(tres[:-1])
    
    c = ChainConsumer()
    for params, fiducial, cov, label in results:
        #print(res[2],res[2].dtype,res[0],res[3])
        chain1 = Chain.from_covariance(fiducial,cov ,columns=params,name=label, linestyle=":")
        c.add_chain(chain1)
    fig = c.plotter.plot()

    pl.savefig('fisher_0.png')
    pl.show()
    #create_triangle_plot(results)

# Run the analysis
#To run for luminosity, BAO and both, can also combine with rd
run_all_cases(interp)
#To run forecast for different number of supernovae
#DL_constraints(interp)

